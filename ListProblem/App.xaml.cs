﻿using Avalonia;
using Avalonia.Markup.Xaml;

namespace ListProblem
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
